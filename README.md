# Subdirectory

Adds a prefix to the URL so the site behaves as if it were installed in a
subdirectory, but without the fun and games of multi-site.

Configure at /admin/config/system/subdirectory
