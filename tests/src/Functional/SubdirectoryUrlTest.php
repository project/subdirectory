<?php

namespace Drupal\Tests\subdirectory\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @coversDefaultClass \Drupal\subdirectory\SubdirectoryUrl
 */
class SubdirectoryUrlTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['toolbar', 'subdirectory'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The currently logged in user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser([
      'access administration pages',
      'view the administration theme',
      'administer site configuration',
      'access toolbar',
    ]);
    $this->drupalLogin($this->user);

    $this->drupalGet('admin/config/system/subdirectory');
    $this->submitForm(['edit-path' => 'catbro'], 'Save configuration');
  }

  /**
   * @covers ::processInbound
   */
  public function testPath() {
    $this->assertSession()->addressEquals('catbro/admin/config/system/subdirectory');
  }

  /**
   * @covers ::processOutbound
   */
  public function testLinks() {
    $this->assertSession()->linkExists('Content');
    $this->clickLink('Content');
    $this->assertSession()->addressEquals('catbro/admin/content');
  }

  /**
   * @covers \Drupal\subdirectory\EventSubscriber\SubdirectoryRedirect::redirectSubdirectory
   */
  public function testRedirect() {
    $this->drupalGet('');
    $this->assertSession()->addressEquals('catbro/user/2');

    $this->drupalGet('admin');
    $this->assertSession()->addressEquals('catbro/admin');
  }

}
