<?php

namespace Drupal\Tests\subdirectory\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @coversDefaultClass \Drupal\subdirectory\SubdirectoryUrl
 */
class SubdirectoryUrlLanguageTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'toolbar',
    'subdirectory',
    'language',
    'node',
    'content_translation',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The currently logged in user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an Article node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType(['type' => 'article']);
    }

    $this->user = $this->drupalCreateUser([
      'administer languages',
      'access administration pages',
      'view the administration theme',
      'administer nodes',
      'create article content',
      'administer site configuration',
      'access toolbar',
      'administer content types',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'edit any article content',
      'administer blocks',
    ]);
    $this->drupalLogin($this->user);

    $this->drupalGet('admin/config/regional/language/add');
    $this->submitForm(['predefined_langcode' => 'de'], 'Add language');

    $this->drupalGet('admin/config/regional/content-language');
    $this->getSession()->getPage()->checkField('edit-entity-types-node');
    $this->getSession()->getPage()->checkField('edit-settings-node-article-translatable');
    $this->submitForm([], 'Save configuration');

    $this->drupalGet('admin/config/system/subdirectory');
    $this->submitForm(['edit-path' => 'catbro'], 'Save configuration');

    $this->placeBlock('local_tasks_block');
    $this->placeBlock('page_title_block');

    $this->drupalGet('admin/config/regional/language/detection/url');
    $this->submitForm(['edit-prefix-en' => 'en'], 'Save configuration');
  }

  /**
   * @covers ::processInbound
   */
  public function testPath() {
    $this->assertSession()->addressEquals('catbro/en/admin/config/regional/language/detection/url');
  }

  /**
   * @covers ::processInbound
   */
  public function testContent() {
    $this->drupalGet('en/admin/structure/types/manage/article');

    $this->drupalGet('en/node/add/article');
    $this->submitForm(['title[0][value]' => 'Test'], 'Save');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('catbro/en/node/1');

    $this->clickLink('Translate');
    $this->clickLink('Add');
    $this->assertSession()->addressEquals('catbro/de/node/1/translations/add/en/de');
    $this->submitForm(['title[0][value]' => 'das test'], 'Save (this translation)');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->addressEquals('catbro/de/node/1');
    $this->assertSession()->elementTextEquals('css', 'h1', 'das test');
  }

}
