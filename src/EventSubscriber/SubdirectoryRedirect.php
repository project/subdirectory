<?php

namespace Drupal\subdirectory\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Redirects paths without the subdirectory prefix.
 */
class SubdirectoryRedirect implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The path to use as a subdirectory.
   *
   * @var null|string
   */
  protected $subdirectoryPath = NULL;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['redirectSubdirectory'];
    return $events;
  }

  /**
   * Constructs a SubdirectoryRedirect object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config, RequestStack $request_stack) {
    $this->subdirectoryPath = $config->get('subdirectory.settings')->get('path');
    $this->requestStack = $request_stack;
  }

  /**
   * Redirect paths that don't have the subdirectory prefix.
   */
  public function redirectSubdirectory() {
    if (!empty($this->subdirectoryPath)) {
      $path = $this->requestStack->getCurrentRequest()->getPathInfo();
      $parts = explode('/', trim($path, '/'));
      $prefix = array_shift($parts);
      if ($prefix !== $this->subdirectoryPath) {
        $response = new RedirectResponse('/' . $this->subdirectoryPath . $path, 302);
        $response->send();
      }
    }
  }

}
