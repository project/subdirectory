<?php

namespace Drupal\subdirectory\Plugin\LanguageNegotiation;

use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\subdirectory\SubdirectoryUrl;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for enabling language prefixing to still function.
 */
class SubdirectoryLanguageNegotiationUrl extends LanguageNegotiationUrl {

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    // Override the URI in the request object with the subdirectory removed so
    // that language negotiation via prefix isn't broken.
    $subdirectory_path = \Drupal::config('subdirectory.settings')->get('path');
    $server_bag = $request->server;
    $uri = $server_bag->get('REQUEST_URI');
    $server_bag->set('REQUEST_URI', SubdirectoryUrl::removePrefix($uri, $subdirectory_path));
    $new_request = $request->duplicate(NULL, NULL, NULL, NULL, NULL, $server_bag->all(), NULL);
    return parent::getLangcode($new_request);
  }

}
