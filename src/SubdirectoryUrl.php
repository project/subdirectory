<?php

namespace Drupal\subdirectory;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for prefixing paths with a virtual subdirectory.
 *
 * @see \Drupal\Core\PathProcessor\PathProcessorAlias
 */
class SubdirectoryUrl implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The path to use as a subdirectory.
   *
   * @var null|string
   */
  protected $subdirectoryPath = NULL;

  /**
   * Constructs a SubdirectoryUrl object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path Matcher service.
   */
  public function __construct(ConfigFactoryInterface $config, PathMatcherInterface $path_matcher) {
    $this->pathMatcher = $path_matcher;
    $this->subdirectoryPath = $config->get('subdirectory.settings')->get('path');
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    return SubdirectoryUrl::removePrefix($path, $this->subdirectoryPath);
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (!empty($this->subdirectoryPath)) {
      $options['prefix'] = $this->subdirectoryPath . '/' . $options['prefix'];
    }
    return $path;
  }

  /**
   * Removes the subdirectory prefix from a given path.
   */
  public static function removePrefix($path, $subdirectory) {
    if (!empty($subdirectory)) {
      $parts = explode('/', trim($path, '/'));
      $prefix = array_shift($parts);
      if ($prefix === $subdirectory) {
        // Remove the subdirectory prefix.
        $path = '/' . implode('/', $parts);
      }
    }
    return $path;
  }

}
