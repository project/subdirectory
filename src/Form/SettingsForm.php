<?php

namespace Drupal\subdirectory\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Settings form for the subdirectory path.
 *
 * @package Drupal\path\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['subdirectory.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subdirectory_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('subdirectory.settings');

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subdirectory Path'),
      '#default_value' => $config->get('path'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('subdirectory.settings');
    $config->set('path', $form_state->getValue('path'));
    $config->save();

    \Drupal::service('router.builder')->rebuild();

    $form_state->setRedirect('subdirectory.settings');

    parent::submitForm($form, $form_state);
  }

}
